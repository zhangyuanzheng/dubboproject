package com.hz.controller;

import com.hz.pojo.HouseInfo;
import com.hz.service.HouseService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/house")
public class HouseController  {

    /**
     * http://localhost:8285/dubbo-admin-2.8.4/
     */


    @Resource
    private HouseService houseService;

    /**
     *
     * @param model
     * @return
     * http://localhost:8092/house/houseInfoList
     */
    @RequestMapping("/houseInfoList")
    public String index(Model model) throws Exception{
        List<HouseInfo> houseInfoList =  houseService.findHouseInfoList();
        model.addAttribute("houseInfoList",houseInfoList);
        return "index";
    }

}
