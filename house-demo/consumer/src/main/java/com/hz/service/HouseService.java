package com.hz.service;

import com.hz.pojo.HouseInfo;

import java.util.List;

/**
 * 本地房屋接口
 */
public interface HouseService {

    /**
     * 查询房屋列表
     * @return
     */
    List<HouseInfo> findHouseInfoList();
}
