package com.hz.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hz.pojo.HouseInfo;
import com.hz.service.HouseService;
import com.hz.service.RpcHouseInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HouseServiceImpl implements HouseService {

    @Reference
    private RpcHouseInfoService rpcHouseInfoService;

    @Override
    public List<HouseInfo> findHouseInfoList() {
        System.out.println("进入消费者..............");
        return rpcHouseInfoService.findHouseInfoList();
    }
}
