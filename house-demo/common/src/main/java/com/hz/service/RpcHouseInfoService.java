package com.hz.service;

import com.hz.pojo.HouseInfo;

import java.util.List;

public interface RpcHouseInfoService {

    /**
     * 查询房屋列表
     * @return
     */
    List<HouseInfo> findHouseInfoList();



}
