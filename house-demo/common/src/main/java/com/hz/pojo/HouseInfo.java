package com.hz.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 房屋实体类
 */
public class HouseInfo implements Serializable {
   private Integer houseId;         //编号
   private String houseDesc;        //描述
   private Integer typeId;          //户型编号
   private String monthlyRent;      //租金
   private Date publishDate;        //发布时间
    private String typeName;        //户型名

    public Integer getHouseId() {
        return houseId;
    }

    public void setHouseId(Integer houseId) {
        this.houseId = houseId;
    }

    public String getHouseDesc() {
        return houseDesc;
    }

    public void setHouseDesc(String houseDesc) {
        this.houseDesc = houseDesc;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getMonthlyRent() {
        return monthlyRent;
    }

    public void setMonthlyRent(String monthlyRent) {
        this.monthlyRent = monthlyRent;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "HouseInfo{" +
                "houseId=" + houseId +
                ", houseDesc='" + houseDesc + '\'' +
                ", typeId=" + typeId +
                ", monthlyRent='" + monthlyRent + '\'' +
                ", publishDate=" + publishDate +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
