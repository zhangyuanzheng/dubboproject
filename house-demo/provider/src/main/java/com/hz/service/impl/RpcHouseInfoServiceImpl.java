package com.hz.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.hz.dao.HouseInfoMapper;
import com.hz.pojo.HouseInfo;
import com.hz.service.RpcHouseInfoService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 远程房屋接口
 */
@Service(interfaceClass = RpcHouseInfoService.class)
@Component
public class RpcHouseInfoServiceImpl implements RpcHouseInfoService {

    @Resource
    private HouseInfoMapper houseInfoMapper;


    @Override
    public List<HouseInfo> findHouseInfoList() {
        System.out.println("这是提供者.................");
        return houseInfoMapper.findHouseInfoList();
    }
}
