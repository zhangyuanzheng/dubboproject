package com.hz.dao;

import com.hz.pojo.HouseInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HouseInfoMapper {
    /**
     * 查询房屋列表
     * @return
     */
    List<HouseInfo> findHouseInfoList();

}
